package com.kun.single_nacos;

import com.alibaba.nacos.api.annotation.NacosInjected;
import com.alibaba.nacos.api.annotation.NacosProperties;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.config.annotation.NacosValue;
import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.api.naming.pojo.Instance;
import com.alibaba.nacos.spring.context.annotation.EnableNacos;
import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@SpringBootApplication
@RestController
@NacosPropertySource(dataId = "nacos-test", autoRefreshed = true) // 加载配置源，dataId为读取的配置id
@EnableNacos(
        globalProperties =
        @NacosProperties(serverAddr = "${nacos.server-addr:localhost:8848}")
)
public class SingleNacosApplication {

    /**
     * 发布配置
     * */
    @NacosInjected
    private ConfigService configService;

    @GetMapping("/publishConfig")
    public String publishConfig(@RequestParam String content) throws NacosException {
        boolean result = configService.publishConfig("nacos-test", "DEFAULT_GROUP", content);
        if (result) {
            return "OK";
        }
        return "FAIL";
    }

    /**
     * 读取配置
     * */
    @GetMapping("/getConfig")
    public String getConfig() throws NacosException {
        return configService.getConfig("nacos-test", "DEFAULT_GROUP", 2000);
    }

    /**
     * 另外一种读取配置
     * */
    @NacosValue(value = "${test.properties.defaultName:无名氏}", autoRefreshed = true)
    private String defaultName;

    @GetMapping("/hi")
    public String hi() {
        return "hi " + defaultName;
    }

    @NacosInjected
    private NamingService namingService;

//    @PostConstruct
//    public void registerInstance() throws NacosException {
//        namingService.registerInstance("test-service", "192.168.0.101", 8888);
//    }

    /**
     * 服务注册
     * */
    @GetMapping("/setInstance")
    public String setInstance() throws NacosException {
        namingService.registerInstance("test-service", "192.168.0.101", 8888);
        namingService.registerInstance("test-service", "192.168.0.102", 8888);
        namingService.registerInstance("test-service", "192.168.0.103", 8888);
        namingService.registerInstance("test-service", "192.168.0.104", 8888);
        return "ok";
    }

    /**
     * 服务发现
     * */
    @GetMapping("/getInstance")
    public List<Instance> getInstance() throws NacosException {
        return namingService.getAllInstances("test-service");
    }

    /**
     * 获取一个健康实例
     * */
    @GetMapping("/getOneInstance")
    public Instance getOneInstance() throws NacosException {
        return namingService.selectOneHealthyInstance("test-service");
    }

    public static void main(String[] args) {
        SpringApplication.run(SingleNacosApplication.class, args);
    }

}
